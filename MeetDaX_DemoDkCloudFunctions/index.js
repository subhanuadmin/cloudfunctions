const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({ 'origin': true });
admin.initializeApp();
var db = admin.firestore();


exports.firstFunction = functions.https.onRequest((req, res) => {
    console.log("Hello");
    console.log(req.query.greeting, req.query.date, req.query.stylistsArr);

    var stylistsArr = JSON.parse(req.query.stylistsArr);
    for (let s = 0; s < stylistsArr.length; s++) {
        console.log(stylistsArr[s]);
    }

    cors(req, res, (request, response) => {
        res.status(200).json(req.query.stylistsArr);
    });


});

exports.freeBusyFlow = functions.https.onRequest((req, res) => {
    var calendarsRef = db.collection('bookings');
    const date = req.query.date;
    const duration = req.query.duration;
    const currentStylistID = req.query.currentStylistID;
    var calendarsArr = [];
    var slotsArr = [];
    var slotsArr2 = [];
    var returnedSlots = [];
    var slots = {
        1: "",
        2: "",
        3: "",
        4: "",
        5: "",
        6: "",
        7: "",
        8: "",
        9: "",
        10: "",
        11: "",
        12: "",
        13: "",
        14: "",
        15: "",
        16: "",
        17: "",
        18: ""
    }
    var timeFrames = {
        0: true,
        1: true,
        2: true,
        3: true,
        4: true,
        5: true,
        6: true,
        7: true,
        8: true,
        9: true,
        10: true,
        11: true,
        12: true,
        13: true,
        14: true,
        15: true,
        16: true,
        17: true,
        18: true,
        19: true,
        20: true,
        21: true,
        22: true,
        23: true,
        24: true,
        25: true,
        26: true,
        27: true,
        28: true,
        29: true,
        30: true,
        31: true,
        32: true,
        33: true,
        34: true,
        35: true,
        36: true,
        37: true,
        38: true,
        39: true,
        40: true,
        41: true,
        42: true,
        43: true,
        44: true,
        45: true,
        46: true,
        47: true,
        48: true,
        49: true,
        50: true,
        51: true,
        52: true,
        53: true,
        54: true,
        55: true,
        56: true,
        57: true,
        58: true,
        59: true,
        60: true,
        61: true,
        62: true,
        63: true,
        64: true,
        65: true,
        66: true,
        67: true,
        68: true,
        69: true,
        70: true,
        71: true,
        72: true,
        73: true,
        74: true,
        75: true,
        76: true,
        77: true,
        78: true,
        79: true,
        80: true,
        81: true,
        82: true,
        83: true,
        84: true,
        85: true,
        86: true,
        87: true,
        88: true,
        89: true,
        90: true,
        91: true,
        92: true,
        93: true,
        94: true,
        95: true,
        96: true,
        97: true,
        98: true,
        99: true,
        100: true,
        101: true,
        102: true,
        103: true,
        104: true,
        105: true,
        106: true,
        107: true,
        108: true,
    }

    console.log("date", date, "currentStylistID", currentStylistID, "duration", duration);

    var allCalendars = calendarsRef.where('stylist', '==', currentStylistID).where('date', '==', date).get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                calendarsArr.push({
                    startTime: doc.data().startTime,
                    endTime: doc.data().endTime
                });
            });

            console.log("calendar array", calendarsArr);

            for (let i = 0; i < calendarsArr.length; i++) {
                const hourStart = parseInt(calendarsArr[i].startTime.substr(0, 2));
                const minuteStart = parseInt(calendarsArr[i].startTime.substr(3, 2));

                const hourEnd = parseInt(calendarsArr[i].endTime.substr(0, 2));
                const minuteEnd = parseInt(calendarsArr[i].endTime.substr(3, 2));

                const temp1 = (hourStart - 9) * 12 + (minuteStart / 5);
                const temp2 = ((hourEnd - 9) * 12 + (minuteEnd / 5));

                for (let i = temp1; i < temp2; i++) {
                    timeFrames[i] = false
                }
            }
            console.log("abc", timeFrames);
            var count = 0
            const dur = duration / 5;
            console.log("dur", dur);

            for (let i = 0; i <= 108; i++) {
                if (timeFrames[i] === true) {
                    count = count + 1;
                    if (count % dur === 0) {
                        slotsArr.push((i + 1) - dur);
                        continue;
                    }
                } else if (timeFrames[i] === false) {
                    count = 0;
                }
            }

            for (let i = 0; i <= 108; i++) {
                var counts = 0;
                //console.log("measures", i , i+dur);
                for (let j = i; j < (i + dur); j++) {
                    if (timeFrames[j] === true)
                        counts = counts + 1;
                }
                //console.log("counts" , i , i+dur, counts); 
                if (counts >= dur) {
                    slotsArr2.push(i);
                }
            }

            console.log("slotsArray2", slotsArr2);

            for (let i = 0; i < 18; i++) {
                for (let j = 0; j < 6; j++) {
                    if (slotsArr2.includes((6 * i) + j)) {
                        slots[i + 1] = (6 * i) + j;
                        var hour = Math.floor(((6 * i) + j) / 12) + 9;
                        var minute = (((6 * i) + j) % 12) * 5;
                        if (hour === 9) {
                            hour = "09";
                        }
                        if (minute === 0) {
                            minute = "00";
                        }
                        const time = hour + ":" + minute;
                        slots[i + 1] = time;
                        break;
                    }
                }
            }

            console.log("proposed slots", slots);

            res.status(200).json(slots);
            return json(slots);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json(slots);
        });
});


exports.freeBusyFlow2 = functions.https.onRequest((req, res) => {
    var stylistsStatusArr = [];
    const date = req.query.date;
    const startTime = req.query.startTime;
    const endTime = req.query.endTime;
    const stylistsArr = JSON.parse(req.query.stylistsArr);
    const bookingsRef = db.collection('bookings');

    console.log("date", date, ", startTime", startTime, ", endTime", endTime, ", stylistsArr", stylistsArr);

    const startTimeMin = (parseInt(startTime.substr(0, 2)) * 60) + parseInt(startTime.substr(3, 2));
    var endTimeMin;

    var bookingsArr = [];

    for (let i = 0; i < stylistsArr.length; i++) {
        stylistsStatusArr.push({
            stylist: stylistsArr[i].stylist,
            status: "free"
        });
    }

    cors(req, res, (request, response) => {
        bookingsRef.where('date', '==', date).get()
            .then(snapshot => {
                snapshot.forEach(doc => {
                    bookingsArr.push(
                        {
                            startTime: doc.data().startTime,
                            endTime: doc.data().endTime,
                            stylist: doc.data().stylist
                        }
                    );
                });

                //console.log(bookingsArr);
                for (let s = 0; s < stylistsArr.length; s++) {
                    endTimeMin = (parseInt(stylistsArr[s].endTime.substr(0, 2)) * 60) + parseInt(stylistsArr[s].endTime.substr(3, 2));
                    for (let b = 0; b < bookingsArr.length; b++) {
                        if (stylistsArr[s].stylist == bookingsArr[b].stylist) {
                            var curStartTimeMin = (parseInt(bookingsArr[b].startTime.substr(0, 2)) * 60) + parseInt(bookingsArr[b].startTime.substr(3, 2));
                            var curEndTimeMin = (parseInt(bookingsArr[b].endTime.substr(0, 2)) * 60) + parseInt(bookingsArr[b].endTime.substr(3, 2));
                            
                            //console.log("curStartTimeMin:"+curStartTimeMin +" startTimeMin:"+ startTimeMin +" curEndTimeMin:"+ curEndTimeMin +" endTimeMin:"+ endTimeMin);
                            if (curStartTimeMin + 1 >= startTimeMin && curStartTimeMin + 1 <= endTimeMin) {
                                stylistsStatusArr[s].status = "busy";

                                break;
                            }
                            else if (curEndTimeMin - 1 >= startTimeMin && curEndTimeMin - 1 <= endTimeMin) {
                                stylistsStatusArr[s].status = "busy";

                                break;
                            }
                            else if (curStartTimeMin < startTimeMin && curEndTimeMin > endTimeMin) {
                                stylistsStatusArr[s].status = "busy";

                                break;
                            }
                            else if (curStartTimeMin > startTimeMin && curEndTimeMin < endTimeMin) {
                                stylistsStatusArr[s].status = "busy";

                                break;
                            }
                            else if (curStartTimeMin == startTimeMin && curEndTimeMin == endTimeMin) {
                                stylistsStatusArr[s].status = "busy";

                                break;
                            }
                        }
                    }
                }
                console.log(stylistsStatusArr);
                res.status(200).json(stylistsStatusArr);

            });
    });
});


exports.sendNotificationForUpdateBooking = functions.firestore
    .document('bookings/{bookingId}')
    .onUpdate((change, context) => {
        const booking = change.after.data();
        const services = [];
        const refs = [];
        var payload = {};
        const customersRef = db.collection('Customers').doc(booking.customer).get();
        const stylistsRef = db.collection('stylist').doc(booking.stylist).get();
        const servicesMenRef = db.collection('services_men').get();
        const servicesWomenRef = db.collection('services_women').get();
        const servicesChildrenRef = db.collection('services_children').get();

        refs.push(customersRef);
        refs.push(stylistsRef);
        refs.push(servicesMenRef);
        refs.push(servicesWomenRef);
        refs.push(servicesChildrenRef);

        Promise.all(refs)
            .then(snapshot => {
                const customer = snapshot[0].data()
                const stylist = snapshot[1].data();
                snapshot[2].forEach(function (doc) {
                    //console.log("ServicesWomen:"+doc.data());
                    for (let s = 0; s < booking.services.length; s++) {
                        //console.log(booking.services[s]+"=="+doc.id);
                        if (booking.services[s] == doc.id)
                            services.push(doc.data().name);
                    }
                });

                snapshot[3].forEach(function (doc) {
                    //console.log("ServicesWomen:"+doc.data());
                    for (let s = 0; s < booking.services.length; s++) {
                        //console.log(booking.services[s]+"=="+doc.id);
                        if (booking.services[s] == doc.id)
                            services.push(doc.data().name);
                    }
                });

                snapshot[4].forEach(function (doc) {
                    //console.log("ServicesChildren:"+doc.data());
                    for (let s = 0; s < booking.services.length; s++) {
                        //console.log(booking.services[s]+"=="+doc.id);
                        if (booking.services[s].id == doc.id)
                            services.push(doc.data().name);
                    }
                });

                console.log("Booking Service:" + services);

                if (services.length == 1) {
                    payload = {
                        notification: {
                            title: 'DemoDk-Booking Update',
                            body: 'Hi ' + customer.firstName + ' ' + customer.lastName + ', your booking has been updated to Stylist: ' + stylist.firstName + ' ' + stylist.lastName + ', Service: ' + services[0] + ', Date: ' + booking.date + ', Time: ' + booking.startTime + '-' + booking.endTime,
                            icon: 'https://firebasestorage.googleapis.com/v0/b/demodk3.appspot.com/o/app%2FlogoBW2-Barbershopdk.png?alt=media&token=f5cf8380-cfce-4ebe-b462-e4ef90cf775e'
                        }
                    };
                }
                else {
                    payload = {
                        notification: {
                            title: 'DemoDk-Booking Update',
                            body: 'Hi ' + customer.firstName + ' ' + customer.lastName + ', your booking has been updated to Stylist: ' + stylist.firstName + ' ' + stylist.lastName + ', Services: ' + services[0] + ', ' + services[1] + ', Date: ' + booking.date + ', Time: ' + booking.startTime + '-' + booking.endTime,
                            icon: 'https://firebasestorage.googleapis.com/v0/b/demodk3.appspot.com/o/app%2FlogoBW2-Barbershopdk.png?alt=media&token=f5cf8380-cfce-4ebe-b462-e4ef90cf775e'
                        }
                    };
                }

                console.log(payload);

                if (customer.uid != "") {
                    customer.fcmToken.forEach(t =>
                        admin.messaging().sendToDevice(t, payload)
                            .then((res) => {
                                console.log("Message sent successfully ", t, res);
                            })
                            .catch((err) => {
                                console.log(err);
                            }))

                }
            })
    });